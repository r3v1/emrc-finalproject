import pandas as pd

def open_dataset():
    """
    Opens Bitcoin OTC trust weighted signed network
    """
    df = pd.read_csv("../data/soc-sign-bitcoinotc.csv")

    # Convert epoch to datetime
    df["TIME"] = df.TIME.apply(lambda x: time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(x)))

    df.set_index(["TIME"], inplace=True)

    return df
