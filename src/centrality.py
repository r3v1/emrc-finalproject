"""
Implementación en Python del módulo `centralization.c` de igraph
junto a algunas medidas de centralidad a nivel de grafo
"""

import networkx as nx
from networkx import algorithms

def centralization(scores, theoretical_max: float, normalize: bool = True):
    """
    Parameters
    ----------
    scores: list, dict
    theoretical_max: float
        The graph level centrality score of the most centralized
        graph with the same number of vertices. Only used if `normalize=True`.
    normalize: bool
        Whether to normalize the centralization
        by dividing the supplied theoretical maximum.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L73
    """
    maxscore = max(scores.values())
    cent = len(scores) * maxscore - sum(scores.values())

    # Normalize
    if normalize:
        cent /= theoretical_max

    return cent

def centralization_degree_tmax(g:nx.Graph, mode: str = "all", loops: bool = False):
    """
    g: nx.Graph
    mode: str
        Constant the specifies the type of degree for directed graphs.
        Options: `in`, `out`, `all`. Default: `all`
    loops: bool
        Whether to consider loops edges when calculating the degree.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L206
    """
    nnodes = g.number_of_nodes()
    if g.is_directed():
        if mode in ["in", "out"]:
            if not loops:
                res = (nnodes - 1) * (nnodes - 1)
            else:
                res = (nnodes - 1) * nnodes
        elif mode == "all":
            if not loops:
                res = 2 * (nnodes - 1) * (nnodes - 2)
            else:
                2 * (nnodes - 1) * (nnodes - 1)
        else:
            raise NotImplementedError(f"Mode {mode} not implemented")
    else:
        if not loops:
            res = (nnodes - 1) * (nnodes - 2)
        else:
            res = (nnodes - 1) * nnodes

    return res


def centralization_degree(g: nx.Graph, normalize: bool = True, mode: str = "all", loops: bool = False):
    """
    Parameters
    ----------
    g: nx.Graph
    normalize: bool
        Whether to normalize the graph level centrality score by dividing
        the supplied theoretical maximum.
    mode: str
        Constant the specifies the type of degree for directed graphs.
        Options: `in`, `out`, `all`. Default: `all`
    loops: bool
        Whether to consider loops edges when calculating the degree.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L128
    """

    tmax = centralization_degree_tmax(g, mode=mode, loops=loops)
    scores = algorithms.degree_centrality(g)

    cent = centralization(scores, tmax, normalize=normalize)

    return cent


def centralization_betweenness_tmax(g: nx.Graph):
    """
    Parameters
    ----------
    g: nx.Graph

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L354
    """
    nnodes = g.number_of_nodes()
    if g.is_directed():
        res = (nnodes - 1) * (nnodes - 1) * (nnodes - 2)
    else:
        res = (nnodes - 1) * (nnodes - 1) * (nnodes - 2) / 2.

    return res

def centralization_betweenness(g: nx.Graph, normalize: bool = True):
    """
    Parameters
    ----------
    g: nx.Graph
    normalize: bool
        Whether to normalize the graph level centrality score by dividing
        the supplied theoretical maximum.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L282
    """

    tmax = centralization_betweenness_tmax(g)
    scores = algorithms.betweenness_centrality(g, k=g.number_of_nodes() // 4, normalized=normalize)

    cent = centralization(scores, tmax, normalize=normalize)

    return cent


def centralization_closeness_tmax(g: nx.Graph):
    """
    Parameters
    ----------
    g: nx.Graph
    
    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L485
    """
        
    nnodes = g.number_of_nodes()
    if g.is_directed():
        res = (nnodes - 1) * (1. - 1.0 / nnodes)
    else:
        res = (nnodes - 1) * (nnodes - 2) / (2.0 * nnodes - 3)

    return res


def centralization_closeness(g: nx.Graph, normalize: bool = True):
    """
    Parameters
    ----------
    g: nx.Graph
    normalize: bool
        Whether to normalize the graph level centrality score by dividing
        the supplied theoretical maximum.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L409
    """        
    tmax = centralization_closeness_tmax(g)
    scores = algorithms.closeness_centrality(g)
    
    cent = centralization(scores, tmax, normalize=normalize)
    
    return cent
    
    
def centralization_eigenvector_tmax(g: nx.Graph, scale: bool = True):
    """
    Parameters
    ----------
    g: nx.Graph
    scale: bool
        Whether to rescale the eigenvector centrality scores, such that the maximum
        score is one.
    
    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L635
    """
    nnodes = g.number_of_nodes()
    
    if g.is_directed():
        res = nnodes - 1
    else:
        if scale:
            res = nnodes - 2
        else:
            raise NotImplementedError(f"Falta por saber qué es `M_SQRT2` en el código original de https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L653")
            # res = (nnodes - 2) / M_SQRT2
            
    return res


def centralization_eigenvector(g: nx.Graph, normalize: bool = True, scale: bool = True):
    """
    Parameters
    ----------
    g: nx.Graph
    normalize: bool
        Whether to normalize the graph level centrality score by dividing
        the supplied theoretical maximum.
    scale: bool
        Whether to rescale the eigenvector centrality scores, such that the maximum
        score is one.

    References
    ----------
    - https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c#L547
    """        
    tmax = centralization_eigenvector_tmax(g, scale=scale)
    scores = algorithms.eigenvector_centrality_numpy(g)
    
    cent = centralization(scores, tmax, normalize=normalize)
    
    return cent