# Estructuras modulares en redes complejas - ICSI/KISA 2020-2021

## Objetivos

El objetivo del proyecto consiste en analizar una red social y plantear un pequeño experimento de predicción a partir de unas características extraídas de la red social en cuestión.

## Instalación

Basta con ejecutar lo siguiente (preferiblemente en un entorno virtual):

```
pip install -r requirements.txt
```

## Ejecución

El fichero principal se trata de [EMRC_DRevillas.ipynb](notebooks/EMRC_DRevillas.ipynb), mientras que los módulos dentro de la carpeta `src` son simplemente para importarlos dentro del cuaderno júpiter.

## Report

El documento explicativo se encuentra en [Proyecto_DRevillas.pdf](docs/Proyecto_DRevillas.pdf).

## Autor

David Revillas - `drevillas002@ikasle.ehu.eus`
