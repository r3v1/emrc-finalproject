\documentclass[journal]{IEEEtran}

%\documentclass[a4paper]{article}
%\usepackage{nips13submit_e, times}
%\nipsfinaltrue

\usepackage{amsmath}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subfig}
\usepackage{biblatex}

\usepackage[utf8]{inputenc}
\usepackage[spanish,es-tabla]{babel}

\usepackage{csquotes}

\title{Estructuras modulares en redes complejas \\ \large Proyecto final}
\author{David Revillas \\ \texttt{drevillas002@ikasle.ehu.eus}}

\bibliography{bib}

\begin{document}
\maketitle

%\begin{abstract}
%    En el siguiente proyecto se estudia la red generada por la plataforma Bitcoin OTC en la realización de transacciones y estudiar \textit{quién confía en quién}.
%\end{abstract}

\section{Introducción}

En el siguiente proyecto se pretende analizar una red social y ser capaz de predecir ciertos comportamientos en función de las características extraídas de dicha red.

Se ha utilizado el conjunto de datos \emph{Bitcoin OTC trust weighted signed network} \cite{kumar2016edge,kumar2018rev2}. En él, se muestra una red de individuos que hicieron transacciones con Bitcoin. Como los usuarios de dicha criptomoneda son, en un principio, anónimos, es necesario llevar un seguimiento de en qué personas se puede confiar para realizar las transacciones y evitar pagos fraudulentos.

Cada transacción ha quedado registrada en la base de datos con la que se ha trabajado, habiendo 35592 aristas entre 5881 nodos. Cada una de éstas aristas es ponderada con un valor entero en el rango $[-10, 10]$, de manera que dicho valor indique la confianza sobre el nodo de destino.

Además, se ha tenidio que reducir dicho conjunto de datos para ser tratado eficazmente a 1500 aristas escogidas aleatoriamente, ya que a partir de dicho número, los cálculos de las distintas características extraídas se extendían en el tiempo.

El objetivo final consistirá en predecir la confianza que deposita un nodo en otro a la hora de realizar la transacción.

\section{Procesamiento}

\subsection{Binarización}

Como se ha dicho, cada una de las transacciones ha sido valorada con un valor entero dentro del rango $[-10, 10]$. Como el objetivo del proyecto es predecir si existirá confianza o no, este problema se puede binarizar, de modo que las aristas con un peso menor o igual que 0, constarán como transacciones inseguras, y por el contrario, los pesos mayores a 0 serán transacciones fiables.


\subsection{Medidas de centralidad}

La librería \texttt{Networkx} de Python implementa muchas medidas de centralidad, sin embargo, a nivel de vértice y no a nivel de grafo.

Formalmente, el término \emph{centralidad} aplicado a todo un grafo no queda definido dentro de la teoría de grafos\cite{freeman1978centrality}. Idealmente, todos los índices de una centralización a nivel de grafo deben tener ciertas características en común:

\begin{itemize}
    \item Deben indexar el grado de la centralidad del punto más central que supere la centralidad del resto de puntos.
    \item Deben expresarse como la proporción de dicho exceso con el máximo valor posible para un grafo conteniendo todos los puntos observados.
\end{itemize}

Entonces, si $n$ es el número de puntos, $C_X(p_i)$ es una centralidad a nivel de vértice, $C_X(p_i\star)$ es el mayor valor de $C_X(p_i)$ para cualquier punto de la red y $\max \sum_{i=1}^{n}\left[ C_X(p_i\star) - C_X(p_i)\right]$ es la máxima suma de las diferencias de centralidades a nivel de vértice para un a grafo de $n$ puntos, entonces:

\begin{equation*}
    C_X = \frac{\sum\limits_{i=1}^{n}\left[ C_X(p_i\star) - C_X(p_i)\right]}{\max \sum\limits_{i=1}^{n}\left[ C_X(p_i\star) - C_X(p_i)\right]}
\end{equation*}

es un índice aceptable. Dicho índice, determinará el grafo el cuál $C_X(p_i\star)$ excede la centralidad de todos los demás puntos de la red y como $C_X$ es una proporción entre una suma de diferencias observadas y un valor máximo, variará entre 0 y 1.


De esta forma, la librería \texttt{igraph} implementa en C las centralidades a nivel de grafo. Es cierto que los desarrolladores de dicha librería también ofrecen respectivas versiones para R y Python. No obstante, para éste último lenguaje, no dispone (o no se han encontrado) medidas de centralidad a nivel de grafo, por lo que se ha decidido reimplementar el fichero original de centralidad\footnote{\url{https://github.com/igraph/igraph/blob/master/src/centrality/centralization.c}} escrito en C, en Python.


Así, se han aplicado las siguientes medidas a un grafo reducido de 1500 aristas aleatorias:
\begin{itemize}
    \item Grado total: número de vecinos de un vértice $u$.
    \item Grado de entrada: vecinos entrantes en un vértice $u$.
    \item Grado de salida: vecinos salientes desde un vértice $u$.
    \item \emph{Betweenness}: porcentaje de relaciones entre pares de nodos en los que la ruta más corta pase por el nodo $u$.
    \item \emph{Closeness}: media de las distancias más cortas hacia un nodo $u$ desde todos nodos alcanzables.
    \item \emph{Eigenvector}: grado de los nodos conectados que a su vez estan conectados a otros.
\end{itemize}

\section{Análisis}

Los siguientes clasificadores se han aplicado al estudio. Todos ellos, con parámetros los parámetros por defecto ofrecidos por la librería \texttt{scikit-learn} y \texttt{Tensorflow}.

\begin{itemize}
    \item \emph{k-Nearest Neighbour} ($k$-NN): clasificador sencillo en el que la etiqueta de un caso se asigna según la etiqueta de los $k$ vecinos más cercanos, en este caso con $k=5$ vecinos.
    \item \emph{Decision Tree} (DT): se genera un modelo construyendo reglas de decisiones simples inferidas mediante las características de los datos.
    \item \emph{Random Forest} (RF): se trata de un método de \emph{ensembling} en el que se entrenan muchos árboles de decisión en distintos subconjuntos de datos, promediándolos.
    \item \emph{SVM}: Método en el que las instancias son llevadas a un espacio de dimensión superior en el que sean linealmente separables.
    \item \emph{Naive Bayes} (NB): clasificador en el que se aplica un el teorema de Bayes asumiendo la independencia condicional de entre todas las pares de variables.
    \item \emph{Red Neuronal} (NN): se trata de la aplicación de transformaciones no lineales al vector de entrada. Sin embargo, esta transformación (los pesos de cada unidad de cada capa) no resulta fácil de aprender. La red planteada cuenta con 4 capas de 128, 64, 32 y 1 unidades de salida respectivamente.
\end{itemize}

\subsection{Gestión del desbalanceo de clases}


El conjunto de datos del que se parte cuenta con un desbalanceo importante después de binarizar los pesos, es decir, los pesos de las aristas positivas se han considerado con un valor de $1$, mientras que pesos negativos con un $0$. Así, 122 de las instancias han sido de clase 0, mientras que 1378 instancias de clase 1. Por ello, se ha tenido que aplicar la ténica SMOTE para rebalancear las instancias. Esta técnica sobremuestrea la clase minoritaria generando puntos intermedios entre dos instancias.


\subsection{Validación}

Se ha realizado una evaluación según una validación cruzada de 10 hojas y realizar una estimación real del error y prevenir el \emph{overfitting}. Se han utilizado las siguientes métricas para dicha validación:

\begin{itemize}
    \item Exactitud: indica la proximidad de los resultados de la observación respecto a los predichos.
    \item Precisión: indica la habilidad del clasificador para etiquetar como positivo instancias que son negativas, $\frac{TP}{TP + FP}$.
    \item \emph{Recall}: indica la habilidad del clasificador de encontrar instsancias positivas, $\frac{TP}{TP + FN}$
    \item F1: puede ser interpretado como una media ponderada entre la precisión y el \emph{recall}.
\end{itemize}


\section{Resultados}

En la Tabla \ref{tab:performance} se muestra el resumen del rendimiento de los clasificadores tras una validación cruzada de 10 hojas, donde se muestra la media y la desviación estándar para cada clasificador. En negrita, el mejor resultado para cada la media y subrayado para la desviación estándar.

%\input{accuracy}
%\input{recall}
%\input{precision}
%\input{f1}


\input{performance}

Se observa que los clasificadores que destacan son el \emph{Random Forest} y el $k$-\emph{Nearest Neighbour}. También se ha realizado un test de normalidad Shapiro-Wilk, en el que se ha rechazado la hipótesis nula $H_0$ ($1.76 \cdot 10^{-6} < 0.01$) con un nivel del $1\%$ de significancia, es decir, no hay información suficiente para suponer que los datos partan de una distribución normal. Por ello, se ha realizado el test de Kruskal-Wallis para comprobar si existe un clasificador significativamente mejor que otros. Del mismo modo, con $\alpha=1\%$, se vuelve a rechazar la hipótesis nula $H_0$ ($4.09 \cdot 10^{-9} < 0.01$) y no se puede decir que exista un mejor clasificador con los datos existentes.


\section{Conclusiones}

Como se ha visto, utilizando sólo 6 medidas de centralidad en estre problema es posible alcanzar un buen comportamiento de clasificadores como en el \emph{Random Forest} y el $k$-\emph{Nearest Neighbour}, modelos fáciles de entrenar y ligeros. Con ello, es posible determinar con cierta fiabilidad si existirá confianza entre dos usuarios de la red.

No obstante, estos resultados podrían mejorarse si se realizara una optimización de los parámetros, ya sea una búsqueda exhaustiva en el espacio de los parámetros o una búsqueda heurística, optimización que no se ha realizado por falta de tiempo. Del mismo modo, establecer una arquitectura correcta ajustando los hiperparámetros de las capas, no es una cuestión sencilla en las redes neuronales, incluso los hiperparámetros del optimizador a utilizar. En este sentido, cabría esperar una mejora trás la aplicación de éstos métodos.

También es importante tener en cuenta la reducción que se ha realizado a la base de datos original, aunque aleatoria, se han tenido en cuenta únicamente 1500 relaciones de las más de 30.000 existentes. Esto podría aportado nueva información que por falta de potencia de cálculo y tiempo no se ha podido efectuar.

\printbibliography
\end{document}


