# Dataset:**Bitcoin OTC trust weighted signed network*

## Dataset information

This is who-trusts-whom network of people who trade using Bitcoin on a platform called Bitcoin OTC. Since Bitcoin users are anonymous, there is a need to maintain a record of users' reputation to prevent transactions with fraudulent and risky users. Members of Bitcoin OTC rate other members in a scale of -10 (total distrust) to +10 (total trust) in steps of 1. This is the first explicit weighted signed directed network available for research.

| Nodes | Edges | Range of edge weight | % of positive edges |
|:-----:|:-----:|:--------------------:|:-------------------:|
|  5881 | 35592 |       -10 to +10     |          89%        |


## Data format

Each line has one rating, sorted by time, with the following format:

`SOURCE, TARGET, RATING, TIME`

where:
- SOURCE: node id of source, i.e., rater
- TARGET: node id of target, i.e., ratee
- RATING: the source's rating for the target, ranging from -10 to +10 in steps of 1
- TIME: the time of the rating, measured as seconds since Epoch. (This can be converted to human readable data easily as described [here](https://stackoverflow.com/questions/3694487/initialize-a-datetime-object-with-seconds-since-epoch))


## Source (citation)

[https://snap.stanford.edu/data/soc-sign-bitcoin-otc.html](https://snap.stanford.edu/data/soc-sign-bitcoin-otc.html)

```
@inproceedings{kumar2016edge,
  title={Edge weight prediction in weighted signed networks},
  author={Kumar, Srijan and Spezzano, Francesca and Subrahmanian, VS and Faloutsos, Christos},
  booktitle={Data Mining (ICDM), 2016 IEEE 16th International Conference on},
  pages={221--230},
  year={2016},
  organization={IEEE}
}

@inproceedings{kumar2018rev2,
  title={Rev2: Fraudulent user prediction in rating platforms},
  author={Kumar, Srijan and Hooi, Bryan and Makhija, Disha and Kumar, Mohit and Faloutsos, Christos and Subrahmanian, VS},
  booktitle={Proceedings of the Eleventh ACM International Conference on Web Search and Data Mining},
  pages={333--341},
  year={2018},
  organization={ACM}
}
```
